﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    internal class Dessert : IPlat
    {
        public readonly string Nom;

        public Dessert(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le dessert " + Nom + "\n";
        }
    }
}