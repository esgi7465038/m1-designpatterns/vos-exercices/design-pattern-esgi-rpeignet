﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    internal class PlatPrincipal : IPlat
    {
        public readonly string Nom;

        public PlatPrincipal(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le plat principal " + Nom + "\n";
        }
    }
}