﻿namespace Menu
{
    internal class PlatPrincipal
    {
        public readonly string Nom;

        public PlatPrincipal(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "-> Je mange le plat principal " + Nom + "\n";
        }
    }
}
