﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleVanne
{
    public class VanneRegulee : IRegulable
    {
        public const int debitMax = 2;
        public bool IsReguleOn { get; set; }

        public VanneRegulee(bool isReguleOn = true) {
            this.IsReguleOn = isReguleOn;
        }

        public int Reguler(int consigneDebit)
        {
            return consigneDebit * 100 / debitMax;
        }

        override
        public string ToString()
        {
            return String.Format("isReguleOn : {0}", this.IsReguleOn.ToString());
        }
    }
}
