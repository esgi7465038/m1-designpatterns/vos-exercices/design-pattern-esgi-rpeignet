﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleVanne
{
    public interface IRegulable
    {
        
        public int Reguler(int consigneDebit);
        bool IsReguleOn { get; set; }
    }
}
