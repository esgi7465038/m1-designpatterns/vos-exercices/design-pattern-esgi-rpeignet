﻿// See https://aka.ms/new-console-template for more information

using ExoMultiplesThreads;

MyThreadLetter myThreadLetter = new MyThreadLetter();
myThreadLetter.Start();
Thread.Sleep(1000);
myThreadLetter.Stop = true;
myThreadLetter.Join();
Environment.Exit(0);