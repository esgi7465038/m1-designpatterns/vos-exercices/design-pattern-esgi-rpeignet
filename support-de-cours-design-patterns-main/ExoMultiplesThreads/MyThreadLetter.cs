﻿namespace ExoMultiplesThreads;

public class MyThreadLetter
{
    private volatile bool stop = false;
    public bool Stop { get => stop; set => stop = value; }

    public char Letter { get; }

    private Thread? LaTache;

    public MyThreadLetter()
    {
        LaTache = new Thread(RunLetter);
        Console.WriteLine("Création de la tache: " + LaTache.GetHashCode());
    }

    public void RunLetter()
    {
        while (!stop)
        {
            stop = true;
        }
        Console.WriteLine("Fin de la tache: " + LaTache.GetHashCode());
    }

    public void Start()
    {
        Console.WriteLine("Démarrage de la tache: " + LaTache.GetHashCode());
        LaTache?.Start();
    }

    public void Join()
    {
        Console.WriteLine("Attente de la fin de la tache: " + LaTache.GetHashCode());
        LaTache?.Join();
    }
}