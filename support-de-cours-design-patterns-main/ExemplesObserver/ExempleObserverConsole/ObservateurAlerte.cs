﻿using ExerciceObserverConsole;
using Observateur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleObserverConsole
{
    public class ObservateurAlerte : IObservateur
    {
        private ConsoleColor ForegroundColor { get; set; }

        public ObservateurAlerte(ConsoleColor foregroundColor)
        {
            ForegroundColor = foregroundColor;
        }

        public void Actualiser(ISujet sujet)
        {
            if(((SujetConcret)sujet).ValObservee > 90)
            {
                ConsoleColor old = Console.ForegroundColor;
                Console.ForegroundColor = ForegroundColor;
                Console.WriteLine("--> " + GetHashCode() + " Val obs: " + ((SujetConcret)sujet).ValObservee);
                Console.ForegroundColor = old;
            }
        }
    }
}
