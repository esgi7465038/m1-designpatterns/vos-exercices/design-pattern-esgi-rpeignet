﻿using Etats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etas
{
    class EtatDepite : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "Pfff Ggrrrrrr (paw!)";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Youpi! J'étais dépité, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            Console.WriteLine("Exception : Je suis déjà dépité, je ne peux pas l'être plus...");
            throw new TransitionImpossibleException("Un étudiant dépité ne peut pas rentrer de vacances.");
        }

        public void Travailler(Etudiant contexte)
        {
            Console.WriteLine("J'étais dépité... je le suis toujours.");
        }
    }
}
