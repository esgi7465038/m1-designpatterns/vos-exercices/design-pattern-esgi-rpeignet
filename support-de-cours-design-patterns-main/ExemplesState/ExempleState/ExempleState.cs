﻿using Etats;

Etudiant UnEtudiant = new Etudiant("Pal");

//  Etat Initial -> Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.PartirEnVacance();
//  Pas de transition, reste Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.RentrerDeVacance();
//  --> Contrarié
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.Travailler();
Console.WriteLine(UnEtudiant.DireBonjour());
// --> Dépité


try
{
    //  Lance une exception TransitionImpossibleException --> un étudiant dépité ne peut pas rentrer de vacances.
    UnEtudiant.RentrerDeVacance();
    Console.WriteLine(UnEtudiant.DireBonjour());
}
catch (TransitionImpossibleException ex)
{
    Console.WriteLine("Exception Transition Impossible: " + ex.Message);
}
finally
{
    //  Toujours Dépité
    Console.WriteLine(UnEtudiant.DireBonjour());
}

