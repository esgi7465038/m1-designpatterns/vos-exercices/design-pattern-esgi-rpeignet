﻿namespace Etats
{
    class EtatContrarie : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "'jour...";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Youpi! J'étais Contrarié, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            Console.WriteLine("Pff, J'suis déjà Contrarié...");
        }

        public void Travailler(Etudiant contexte)
        {
            Console.WriteLine("J'ai du travail a faire...");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatDepite];
        }
    }
}
