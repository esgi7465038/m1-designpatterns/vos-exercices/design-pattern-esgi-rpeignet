﻿namespace PersonneStrategyDeplacement
{
    internal class Personne
    {
        public string Nom { get; }
        public int Age { get; }

        /// <summary>
        /// L'association avec un mode de déplacement et sa property
        /// </summary>
        private IStrategyDeplacement? deplacement = null;
        public IStrategyDeplacement? Deplacement { get => deplacement; set => deplacement = value; }

        public Personne(string nom, int age)
        {
            Nom = nom;
            Age = age;
        }

        /// <summary>
        /// Efefctue le déplacement selon le mode passé en paramètre
        /// </summary>
        /// <param name="modeDeplacement"> 1 pour marcher, 2 pour courrir</param>
        /// <returns>le message de déplacement</returns>
        public string SeDeplacer()
        {
            string ret = "";

            if (deplacement != null)
            {
                ret = Nom + deplacement.DoDeplacement();
            }

            return ret;
        }
    }
}
