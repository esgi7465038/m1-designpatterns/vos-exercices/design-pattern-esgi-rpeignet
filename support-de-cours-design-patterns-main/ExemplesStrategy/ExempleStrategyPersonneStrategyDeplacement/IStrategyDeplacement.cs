﻿namespace PersonneStrategyDeplacement
{
    internal interface IStrategyDeplacement
    {
        public string DoDeplacement();
    }
}
