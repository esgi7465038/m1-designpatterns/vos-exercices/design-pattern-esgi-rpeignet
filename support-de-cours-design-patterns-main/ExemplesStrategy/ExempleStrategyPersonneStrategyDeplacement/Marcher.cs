﻿namespace PersonneStrategyDeplacement
{
    internal class Marcher : IStrategyDeplacement
    {
        public string DoDeplacement()
        {
            return " se déplace en marchant.";
        }
    }
}
