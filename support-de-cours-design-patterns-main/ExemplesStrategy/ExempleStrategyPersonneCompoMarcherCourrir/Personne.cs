﻿namespace PersonneCompoMarcherCourrir
{
    internal class Personne
    {
        public string Nom { get; }
        public int Age { get; }

        /// <summary>
        /// Composition avec les classe encapsulant les déplacements
        /// </summary>
        private Marcher marcher = new Marcher();
        private Courrir courrir = new Courrir();

        public Personne(string nom, int age)
        {
            Nom = nom;
            Age = age;
        }

        /// <summary>
        /// Efefctue le déplacement selon le mode passé en paramètre
        /// </summary>
        /// <param name="modeDeplacement"> 1 pour marcher, 2 pour courrir</param>
        /// <returns>le message de déplacement</returns>
        public string SeDeplacer(int modeDeplacement=1)
        {
            string ret = "";

            switch (modeDeplacement)
            {
                case 1: 
                    ret = Nom + marcher.DoDeplacement();
                    break;
                case 2:
                    ret = Nom + courrir.DoDeplacement();
                    break;
            }
            return ret;
        }
    }
}
