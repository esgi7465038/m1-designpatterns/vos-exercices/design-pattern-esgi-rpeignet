﻿using System;

namespace ExempleSingletonSimple
{
    internal class CSingleton
    {
        //  La référence statique de la seule instance
        private static CSingleton? objetUnique = null;

        //  Une propriété.
        public string UnMessage => "JE SUIS UNIQUE! La preuve avec mon HashCode: " + this.GetHashCode().ToString();

        //  Le constructeur privé
        private CSingleton()
        {
            Console.WriteLine("Constructeur " + UnMessage);
        }

        //  La méthode d'accès à l'instance objetUnique
        public static CSingleton ObjetUnique
        {
            get
            {
                //if (objetUnique == null)
                //    //  Si objetUnique n'existe pas, on le crée
                //    objetUnique = new CSingleton();
                //  Si objetUnique n'existe pas, on le crée
                objetUnique ??= new CSingleton();

                //  On retourne l'instance existante, éventuellement fraichement crée.
                return objetUnique;
            }
        }
    }
}
