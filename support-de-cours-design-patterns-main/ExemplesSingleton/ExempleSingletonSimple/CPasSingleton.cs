﻿using System;

namespace ExempleSingletonSimple
{
    internal class CPasSingleton
    {
        //  Une petite classe "normale" n'implémentant pas le DP Singleton.
        public String UnMessage
        {
            get { return "Je suis pas unique! La preuve avec mon HashCode: " + this.GetHashCode().ToString(); }
        }

        public CPasSingleton()
        {
            Console.WriteLine("Constructeur " + UnMessage);
        }
    }
}
