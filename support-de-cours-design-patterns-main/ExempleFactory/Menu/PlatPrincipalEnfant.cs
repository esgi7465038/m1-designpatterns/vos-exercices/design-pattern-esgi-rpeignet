﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class PlatPrincipalEnfant : PlatPrincipalAbstract
    {
        public PlatPrincipalEnfant(string nom)
        {
            Nom = nom;
        }

        public override string Manger()
        {
            return "=> Je mange le plat principal " + Nom + " en tant qu'enfant\n";
        }
    }
}
