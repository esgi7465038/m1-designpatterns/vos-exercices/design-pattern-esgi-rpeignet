﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class DessertEnfant : DessertAbstract
    {
        public DessertEnfant(string nom)
        {
            Nom = nom;
        }

        public override string Manger()
        {
            return "=> Je mange le dessert " + Nom + " en tant qu'enfant\n";
        }
    }
}
