﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class PlatPrincipal : PlatPrincipalAbstract
    {
        public PlatPrincipal(string nom)
        {
            Nom = nom;
        }
    }
}
