﻿using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleFactory.Menu
{
    public abstract class EntreeAbstract : IPlat
    {
        public string Nom;
        public virtual string Manger()
        {
            return "=> Je mange l'entrée " + Nom + "\n";
        }
    }
}
