﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class Dessert : DessertAbstract
    {
        public Dessert(string nom)
        {
            Nom = nom;
        }
    }
}
