﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class EntreeEnfant : EntreeAbstract
    {
        public EntreeEnfant(string nom) 
        { 
            Nom = nom;
        }

        public override string Manger()
        {
            return "=> Je mange l'entrée " + Nom + " en tant qu'enfant\n";
        }
    }
}
