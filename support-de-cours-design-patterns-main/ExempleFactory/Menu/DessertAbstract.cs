﻿using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleFactory.Menu
{
    public abstract class DessertAbstract : IPlat
    {
        public string Nom;
        public virtual string Manger()
        {
            return "=> Je mange le dessert " + Nom + "\n";
        }
    }
}
