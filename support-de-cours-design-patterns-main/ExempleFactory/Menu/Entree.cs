﻿using ExempleFactory.Menu;

namespace Menu
{
    internal class Entree : EntreeAbstract
    {
        public Entree(string nom) 
        { 
            Nom = nom;
        }
    }
}
