﻿using ExempleFactory.Factory;
using Factory;
using Menu;

//  Création de la fabrique
/*PlatFactory factory = new PlatFactory();

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entree = factory.CreateEntree("Salade verte");
PlatPrincipal platPrincipal = (PlatPrincipal)factory.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(factory.CreateDessert("Tiramizu"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { factory.CreateEntree("Nems"),
    factory.CreateEntree("Giozza"),
    factory.CreatePlatPrincipal("Bobun"),
    factory.CreateDessert("Moshi"),
    factory.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());*/


Console.WriteLine("*************************");
Console.WriteLine("* Menu avec PlatFactoryEnfant *");
Console.WriteLine("*************************");
PlatEnfantFactory factoryEnfant = new PlatEnfantFactory();

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entree = factoryEnfant.CreateEntree("Tomate cerise");
PlatPrincipalEnfant platPrincipal = (PlatPrincipalEnfant)factoryEnfant.CreatePlatPrincipal("Burger frite");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(factoryEnfant.CreateDessert("Mousse au chocolat"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { factoryEnfant.CreateEntree("Nems"),
    factoryEnfant.CreateEntree("Giozza"),
    factoryEnfant.CreatePlatPrincipal("Bobun"),
    factoryEnfant.CreateDessert("Moshi"),
    factoryEnfant.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());


Console.WriteLine("*************************");
Console.WriteLine("* Menu avec PlatFactoryAdulte *");
Console.WriteLine("*************************");
PlatAdulteFactory factoryAdulte = new PlatAdulteFactory();

//  Création du menu
Menu.Menu menuAdulte = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entreeAdulte = factoryAdulte.CreateEntree("Salade verte");
PlatPrincipal platPrincipalAdulte = (PlatPrincipal)factoryAdulte.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menuAdulte.AddPlat(entreeAdulte).AddPlat(platPrincipalAdulte).AddPlat(factoryAdulte.CreateDessert("Tiramizu"));
Console.WriteLine(menuAdulte.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] platsAdulte = { factoryAdulte.CreateEntree("Nems"),
    factoryAdulte.CreateEntree("Giozza"),
    factoryAdulte.CreatePlatPrincipal("Bobun"),
    factoryAdulte.CreateDessert("Moshi"),
    factoryAdulte.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menuAdulte2 = new Menu.Menu(2, platsAdulte);
Console.WriteLine(menuAdulte2.Consommer());