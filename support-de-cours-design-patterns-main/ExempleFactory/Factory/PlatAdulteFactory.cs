﻿using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleFactory.Factory
{
    internal class PlatAdulteFactory : PlatFactoryAbstract
    {
        public override IPlat CreateEntree(string nom)
        {
            return new Entree(nom);
        }

        public override IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipal(nom);
        }

        public override IPlat CreateDessert(string nom)
        {
            return new Dessert(nom);
        }
    }
}
