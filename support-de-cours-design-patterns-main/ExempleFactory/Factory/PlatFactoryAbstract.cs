﻿using Menu;

namespace ExempleFactory.Factory
{
    public abstract class PlatFactoryAbstract
    {
        public virtual IPlat CreateEntree(string nom)
        {
            return new Entree(nom);
        }

        public virtual IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipal(nom);
        }

        public virtual IPlat CreateDessert(string nom)
        {
            return new Dessert(nom);
        }
    }
}
