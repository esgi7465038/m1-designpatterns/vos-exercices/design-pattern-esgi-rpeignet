﻿using System;
using System.Diagnostics;
using System.Threading;

namespace HelloThread
{
    class HelloThread
    {
        /// <summary>
        /// Un booléen pour arréter le thread.
        /// </summary>
        public static bool stop = false;

        #region thread
        /// <summary>
        /// La méthode s'exécutant en tant que Thread.
        /// </summary>
        public static void RunLetter()
        {
            //  Pour mesurer le temps
            var sw = Stopwatch.StartNew();

            // Un random pour le sleep...
            Random rnd = new();

            // La lettre à afficher
            char letter = 'a';

            Console.WriteLine("\t\t---> Le RunLetter commence son travail, id {0}, état {1}, priorité {2}.",
                        Environment.CurrentManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            //while (true)
            while (!stop)
            {
                if (letter < 'z')
                    letter++;
                else
                    letter = 'a';

                Console.WriteLine("\t\t---> Letter: " + letter.ToString() + " à " + sw.ElapsedMilliseconds / 1000.0 + " secondes.");

                //  Dodo pour un temps aléatoire < 1267ms
                Thread.Sleep(rnd.Next(1267));
            }
            sw.Stop();
            Console.WriteLine("\t\t---> Le RunLetter a fini son travail.");
        }
        #endregion

        static void Main(string[] args)
        {
            //  La durée en seconde du thread principal
            int dureeMain = 20;

            // Le thread lui même, création sans le démarrer
            //ThreadStart delegateRunLetter = new ThreadStart(RunLetter);
            //Thread LaTache = new Thread(delegateRunLetter);
            Thread LaTache = new(RunLetter);

            //  Démarrage des threads
            Console.WriteLine("> Le thread est créé.\n> Démarrage...");
            LaTache.Start();

            //  Le thread principal compte le temps
            Console.WriteLine("> Main " + Environment.CurrentManagedThreadId + " en route pour {0} secondes...", dureeMain);
            for (int i = 1; i < dureeMain; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> main " + Environment.CurrentManagedThreadId + ": " + i + "sec.");
            }

            //  Arrêt du thread LaTache
            Console.WriteLine("> Demmande d'arret des threads.");
            //  Beurk! Abort c'est mal !!
            //LaTache.Abort();
            stop = true;

            //  On attend que le thread en cours soit effectivement terminé avant de continuer.
            LaTache.Join();
            Console.WriteLine("> Le thread LaTache est terminé.");

            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();
        }
    }
}
