﻿// See https://aka.ms/new-console-template for more information
using ExoStrategyFoot.action;
using ExoStrategyFoot.player;

Console.WriteLine("Création de l'équipe de foot");
Tir tir = new Tir();
Passe passe = new Passe();
Drible drible = new Drible();

Console.WriteLine("Création des actions");
Ailier ailier = new Ailier(10);
Defenseur defenseur = new Defenseur(8);
Milieu milieu = new Milieu(7);

Console.WriteLine("Attribution des actions aux joueurs");
ailier.PlayingAction = tir;
defenseur.PlayingAction = passe;
milieu.PlayingAction = drible;

Console.WriteLine(ailier.doAction());
Console.WriteLine(defenseur.doAction());
Console.WriteLine(milieu.doAction());