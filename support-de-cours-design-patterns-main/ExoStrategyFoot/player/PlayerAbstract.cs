﻿using ExoStrategyFoot.action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoStrategyFoot.player
{
    public abstract class PlayerAbstract
    {
        int Numero {  get; set; }
        public IPlayingAction PlayingAction { get; set; }
        protected PlayerAbstract(int numero) {
            Numero = numero;
        }

        public virtual string doAction()
        {
            return PlayingAction.actionToDo();
        }
    }
}
